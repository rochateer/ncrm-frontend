import React from 'react';
import { Col, Row } from 'reactstrap';
import Flatpickr from "react-flatpickr";
import { createSelector } from 'reselect';
import { useSelector } from 'react-redux';

import { datetimeformatiso } from '../../helpers/FunctionalHelper/date-helper';
import { datetimeformatncrm, dateformatncrm, datemonthformatncrm } from "../../helpers/FunctionalHelper/date-helper";

const Section = (props) => {

    const [dataForm, setDataForm] = React.useState({});
    const [userName, setUserName] = React.useState("Admin");
    const [currentDate, setCurrentDate] = React.useState([]);

    const current_date = new Date();
    const next_day_date = new Date(new Date().setDate(new Date().getDate()));
    const next_month_date = new Date(next_day_date.setMonth(next_day_date.getMonth() + 1));
    const previous_day_date = new Date(new Date().setDate(new Date().getDate() + 1));
    const previous_month_date = new Date(previous_day_date.setMonth(previous_day_date.getMonth() - 1));

    const profiledropdownData = createSelector(
        (state) => state.Profile.user,
        (user) => user
      );
    // Inside your component
    const user = useSelector(profiledropdownData);

    const setNewDate = (e) => {
        const dateData = {...dataForm};
        if(e !== undefined && e.e[0] !== undefined && e.e[1] !== undefined){
            dateData["start_date"] = e.e[0]
            dateData["end_date"] = e.e[1]
            props.searchDate(dateData)
        }

        setDataForm(dateData)
    }

    React.useEffect(() => {
        if (sessionStorage.getItem("authUser")) {
            const obj = JSON.parse(sessionStorage.getItem("authUser"));
            setUserName(process.env.REACT_APP_DEFAULTAUTH === "fake" ? obj.username === undefined ? user.first_name ? user.first_name : obj.data.first_name : "Admin" || "Admin" :
                process.env.REACT_APP_DEFAULTAUTH === "firebase" ? obj.email && obj.email : "Admin"
            );
        }
    }, [userName, user]);

    return (
        <React.Fragment>
            <Row className="mb-3 pb-1">
                <Col xs={12}>
                    <div className="d-flex align-items-lg-center flex-lg-row flex-column">
                        <div className="flex-grow-1">
                            <h4 className="fs-16 mb-1">Hello, {userName}!</h4>
                            <p className="text-muted mb-0">Here's what's happening with your dashboard today.</p>
                        </div>
                        {/* <div className="mt-3 mt-lg-0">
                            <form action="#">
                                <Row className="g-3 mb-0 align-items-center">
                                    <div className="col-sm-auto">
                                        <div className="input-group">
                                            <Flatpickr
                                                className="form-control border-0 dash-filter-picker shadow"
                                                options={{
                                                    mode: "range",
                                                    dateFormat: "d M, Y",
                                                    defaultDate: [previous_month_date, next_month_date],
                                                    onChange: (e) => {setNewDate({e})}
                                                }}
                                            />
                                            <div className="input-group-text bg-primary border-primary text-white"><i className="ri-calendar-2-line"></i></div>
                                        </div>
                                    </div>
                                </Row>
                            </form>
                        </div> */}
                    </div>
                </Col>
            </Row>
        </React.Fragment>
    );
};

export default Section;