import React from 'react';
import { Card, CardHeader, Col, DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from 'reactstrap';
import { teamPendingApprovalTasks } from '../../common/data';
import { TeamPendingApprovalCharts } from './DashboardPieCharts';

import avatardummy from "../../assets/images/users/user-dummy-img.jpg";

import { API_CLIENT_AUTH_BEARER } from '../../helpers/api-helper';
const apiAuth = new API_CLIENT_AUTH_BEARER();

const TeamPendingApproval = () => {
    const [dataList, setDataList] = React.useState([]);
    const userData = JSON.parse(sessionStorage.getItem("authUser")) ? JSON.parse(sessionStorage.getItem("authUser")).data : {};

    // console.log("User Data", userData);

    const getData = async() => {
        let where = '{"lm_email" : "'+userData.email+'"}';
        const res = await apiAuth.get('/getUser?q='+where+'&noPg=1');
        if(res && res.data){
            const data_list = res.data.data;
            const data_list_filter = data_list.filter(dt => dt.role.includes("Change Coordinator"));
            setDataList(data_list_filter);
            // setPerPage(res.perPage)
        }
    }
    
    React.useEffect(() => {
        getData();
    }, []);
    
    return (
        <React.Fragment>
            <Col md={12}>
                <Card>
                    <CardHeader className="align-items-center d-flex bg-primary text-white">
                        <h4 className="card-title mb-0 flex-grow-1 text-white">Pending Approval</h4>
                    </CardHeader>

                    <div className="card-body">

                        <div className="table-responsive table-card" style={{minHeight: "85px", maxHeight: "375px"}}>
                            <table className="table table-borderless table-nowrap align-middle mb-0" >
                                <thead className="table-light text-muted">
                                    <tr>
                                        <th scope="col">Member</th>
                                        <th scope="col">Pending Approval</th>
                                        {/* <th scope="col">Status</th> */}
                                    </tr>
                                </thead>
                                <tbody>
                                    {(dataList || []).map((item, key) => (
                                    <tr key={key}>
                                        <td className="d-flex">
                                            <img src={avatardummy} alt="" className="avatar-xs rounded-3 me-2" />
                                            <div>
                                                <h5 className="fs-13 mb-0">{item.first_name + " " + item.last_name}</h5>
                                                <p className="fs-12 mb-0 text-muted">{item.title}</p>
                                            </div>
                                        </td>
                                        <td style={{textAlign: "center"}}>
                                            0
                                        </td>
                                        {/* <td><span className={"badge bg-muted-subtle text-muted"}>Offline</span></td> */}
                                    </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </Card>
            </Col>
        </React.Fragment>
    );
};

export default TeamPendingApproval;