import React from 'react';
import CountUp from "react-countup";
import { Link } from 'react-router-dom';
import { Card, CardBody, Col } from 'reactstrap';
import { ecomWidgets } from "../../common/data";
import { dataWidget } from './data-common';

import { datetimeformatncrm, dateformatncrm, datemonthformatncrm, dateyearsmonthdayformatncrm } from "../../helpers/FunctionalHelper/date-helper";

import { API_CLIENT_AUTH_BEARER } from '../../helpers/api-helper';

const apiAuth = new API_CLIENT_AUTH_BEARER();

const PendingApprovalWidgets = () => {

    const isUserExpertDomain = (user_data) => {
        if(user_data.title && user_data.title.includes("Expert Domain")){
            return true
        }else{
            return false
        }
    }

    const isUserChangeManager = (user_data) => {
        if(user_data.title && user_data.title.includes("Manager") && user_data.title.includes("Change and Release")){
            return true
        }else{
            return false
        }
    }

    const [dataList, setDataList] = React.useState([]);
    const [totalData, setTotalData] = React.useState(0);
    const [totalPendingApproval, setTotalPendingApproval] = React.useState(0);

    const userData = JSON.parse(sessionStorage.getItem("authUser")) ? JSON.parse(sessionStorage.getItem("authUser")).data : {};
    // console.log("User Data Pending", userData)

    const current_date = new Date();
    const next_day_date = new Date(new Date().setDate(new Date().getDate()));
    const next_month_date = new Date(next_day_date.setMonth(next_day_date.getMonth() + 1));
    const previous_day_date = new Date(new Date().setDate(new Date().getDate() + 1));
    const previous_month_date = new Date(previous_day_date.setMonth(previous_day_date.getMonth() - 1));

    const getData = async() => {
        let filter_array = [];

        let where = '{}';

        if(userData.role && userData.role.find(role => role.includes("Requestor") || role.includes("Change Coordinator")) !== undefined){
            filter_array.push('"$or" : [{"created_by" : "'+userData.email+'"},{"current_status" : "Request for Authorization", "change_coordinator_email" : "'+userData.email+'"},{"current_status" : "Request for Change", "change_coordinator_email" : "'+userData.email+'"}]');
        }else if(userData.title && (userData.title.includes("Expert Domain") === true || userData.title.includes("Domain Expert") === true)){
            filter_array.push('"$or" : [{"current_status" : "Scheduled for Approval", "change_category" : "Normal Major"},{"current_status" : "Scheduled for Approval", "change_category" : "Normal Significan"}]');
        }

        if(filter_array){
            where = '{' + filter_array.join(',') + '}';
        }

        
        const res = await apiAuth.get('/getCRM?q='+where+'&noPg=1');

        if(res && res.data){
            // console.log("Res Data Pending", res.data)
            setDataList(res.data.data)
            setTotalData(res.data.totalResults)
            // setPerPage(res.perPage)
        }
    }
    
    React.useEffect(() => {
        getData();
    },[]);

    const pendingApproval = async() => {
        let data_total = totalPendingApproval;
        let data_draft = 0;
        let data_rfa = 0;
        let data_rfc = 0;
        let data_sfa = 0;

        if(dataList !== undefined){
            if(userData.role && userData.role.find(role => role.includes("Change Coordinator")) !== undefined){
                data_rfa =+ (dataList.filter(dt => dt.current_status === "Request for Authorization" && dt.change_coordinator_email === userData.email )).length
                data_rfc =+ (dataList.filter(dt => dt.current_status === "Request for Change" && dt.change_coordinator_email === userData.email )).length

                // console.log("Pending RFA", dataList.filter(dt => dt.current_status === "Request for Authorization" && dt.change_coordinator_email === userData.email ))
                // console.log("Pending RFC", dataList.filter(dt => dt.current_status === "Request for Change" && dt.change_coordinator_email === userData.email ))
            }
            if(userData.title && (userData.title.includes("Expert Domain") === true || userData.title.includes("Domain Expert") === true)){
                data_sfa =+ (dataList.filter(dt => dt.current_status === "Scheduled for Approval" && (dt.change_category === "Normal Major" || dt.change_category === "Normal Significant" ))).length

                // console.log("Pending SFA", dataList.filter(dt => dt.current_status === "Scheduled for Approval" && (dt.change_category === "Normal Major" || dt.change_category === "Normal Significant" )))
            }
            if(isUserExpertDomain(userData) ){
                let data_temp = dataList.filter(dt => dt.current_status === "Scheduled for Approval")
                data_temp = data_temp.filter(dt => dt.sfa_approvals && (dt.sfa_approvals.find(sfa => sfa.role === "Expert Domain") !== undefined || dt.sfa_approvals.find(sfa => sfa.role === "Domain Expert") !== undefined));
                data_sfa = data_temp.length
                console.log("test", data_sfa)
            }
        }

        data_total = data_draft + data_rfa + data_rfc + data_sfa;

        setTotalPendingApproval(data_total);
    } 
    
    React.useEffect(() => {
        pendingApproval();
    },[dataList]);

    // console.log("Data Pending Approval",  totalPendingApproval);

    return (
        <React.Fragment>
            {totalPendingApproval !== 0 ?
                <Col xl={12} md={12} >
                    <Link to={"/cr-list/pending/"+userData.email}>
                    <Card className={"card-animate bg-info"}>
                        
                        <CardBody>
                            
                            <div className="d-flex align-items-center">
                                <div className="flex-grow-1">
                                    <h5 className={"text-uppercase fw-medium mb-0 text-white-50"}>Pending Approval</h5>
                                </div>
                                <div className="flex-shrink-0">
                                </div>
                            </div>
                            <div className="d-flex align-items-end justify-content-between mt-4">
                                <div>
                                    <h4 className={"fs-22 fw-semibold ff-secondary mb-4 text-white"}><span className="counter-value" data-target="559.25">
                                        <CountUp
                                            start={0}
                                            prefix={""}
                                            suffix={""}
                                            separator={""}
                                            end={totalPendingApproval}
                                            decimals={0}
                                            duration={4}
                                        />
                                    </span></h4>
                                </div>
                                <div className="avatar-sm flex-shrink-0">
                                    <span 
                                    className={"avatar-title rounded fs-3 bg-opacity-25 bg-white bg-opacity-25"}
                                    >
                                        <i className={"bx bxs-user-account text-white"}></i>
                                    </span>
                                </div>
                            </div>
                        </CardBody>
                    </Card>
                    </Link>
                </Col>
                : ""
            }
        </React.Fragment>
    );
};

export default PendingApprovalWidgets;