import React from 'react';
import { Card, CardHeader, CardBody, Col, Container, Form, Input, Label, Row, Button } from 'reactstrap';
import Select from "react-select";

// Import React FilePond
import { FilePond,  registerPlugin } from 'react-filepond';
// Import FilePond styles
import 'filepond/dist/filepond.min.css';
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';

import { API_CLIENT_AUTH_BEARER } from '../../../helpers/api-helper';
import { CR_CURRENT_STATUS_CAN_EDIT } from '../../../config/data/CR_CURRENT_STATUS_CAN_EDIT';

const apiAuth = new API_CLIENT_AUTH_BEARER();

// Register the plugins
registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

const AttachmentNameOptions = [
    { value: 'Method of Procedure', label: 'Method of Procedure' },
    { value: 'Root Cause Analys', label: 'Root Cause Analys' },
    { value: 'Supporting Attachment', label: 'Supporting Attechment' },
    { value: 'Timeline', label: 'Timeline' },
    { value: 'Evidence', label: 'Evidence' },
    { value: 'Additional Information', label: 'Additional Information' },
    { value: 'Implementation Procedure', label: 'Implementation Procedure' },
];

const CRUpdateClassification = (props) => {

    const [optionActivityList, setOptionActivityList] = React.useState([]);

    const [dataForm, setDataForm] = React.useState({
        file_attacment : [
            {
                "attachment_name" : "Method of Procedure",
                "file" : null,
                "size" : null,
            },{
                "attachment_name" : "Evidence",
                "file" : null,
                "size" : null,
            },{
                "attachment_name" : "Supporting Attachment",
                "file" : null,
                "size" : null,
            }
        ]
    });
    const [selectedForm4, setSelectedForm4] = React.useState(null);
    const [dataFiles, setDataFiles] = React.useState([]);
    const [dataAddSub, setDataAddSub] = React.useState([{
        "attachment_name" : "Method of Procedure",
        "file" : null,
        "size" : null,
    },{
        "attachment_name" : "Evidence",
        "file" : null,
        "size" : null,
    },{
        "attachment_name" : "Supporting Attachment",
        "file" : null,
        "size" : null,
    }]);

    const Activity_List_Select_Option = props.activityList;

    let Activity_Select_Option = [...new Set(Activity_List_Select_Option.filter(item => item["Categorization Tier 3__c"] === props.dataForm["oc_tier_3"]).map(item => item["Activity Description__c"]))].map(item => ( {value: item, label: item} ))

    const handleInputChange = (e) => {
        const dataFormInput = {...dataForm}
        const {name, value} = e.target
        dataFormInput[name] = value;
        props.onChangeForm(name, value);
        setDataForm(dataFormInput)
    }

    const handleSelectForm = (e, name) => {
        const data_form = {...dataForm};
        const {value, label, impact, risk, change_category} = e;
        data_form[name] = value;
        if(name === "activity_list"){
            data_form["change_category"] = change_category;
            data_form["impact"] = impact;
            data_form["risk"] = risk;
            props.onChangeFormAll(data_form);
        }else{
            props.onChangeForm(name, value);
        }
        setDataForm(data_form);
    }

    const handleSelectInputFileForm = (e, name, index) => {
        const file_data = [...dataAddSub];
        const {value, label} = e;
        file_data[index]["attachment_name"] = value;
        const data_form = {...dataForm};
        data_form["file_attacment"] = dataAddSub;
        props.addAttachment(data_form.file_attacment);
        setDataAddSub(file_data);
        setDataForm(data_form);
    }

    const handleInputFileForm = (e, name, index) => {
        const fileUpload = {...dataFiles};
        const file_data = e.target.files[0];

        // console.log("Data File Upload", file_data);

        // application/pdf means .pdf
        // application/msword means .doc
        // application/vnd.openxmlformats-officedocument.wordprocessingml.document means .docx
        // application/vnd.ms-excel means .xls

        setDataFiles([...dataFiles,{
            "index" : index,
            "size" : file_data?.size,
            "value" : file_data
        }])

        dataAddSub[index]["file"] = file_data;
        dataAddSub[index]["size"] = file_data?.size;

        const data_form = {...dataForm};
        data_form["file_attacment"] = dataAddSub;
        
        setDataAddSub(dataAddSub);
        props.addAttachment(data_form.file_attacment);
        setDataForm(data_form);
    };

    const delCrAttachment = (index) => {
        const file_data = [];
    
        for(let i=0 ; i<dataAddSub.length ; i++){
          if(i !== index){
            file_data.push(dataAddSub[i]);
          }
        }
        
        setDataAddSub(file_data);
        const data_form = {...dataForm};
        data_form["file_attacment"] = file_data;
        props.addAttachment(data_form.file_attacment)
        setDataForm(data_form);
      }

    const addCrAttachment = (ele) => {
        const dataSubAttachment = [
            ...dataAddSub,
            {
                "attachment_name" : null,
                "file" : null,
                "size" : null,
            }
        ]
        setDataAddSub(dataSubAttachment);
    };

    const getDataActivityList = async() => {
        let oc_tier_1 = dataForm.oc_tier_1;
        if(oc_tier_1){
            if(oc_tier_1.includes('&')){
                oc_tier_1 = oc_tier_1.split(' & ')[1]
            }
        }
        const res = await apiAuth.get('/getActivityList?&lmt=100&q={"categorization_tier_1" : {"$substring" : "'+oc_tier_1+'"}, "domain_category" : "'+dataForm.oc_domain_category+'", "categorization_tier_2" : "'+dataForm.oc_tier_2+'" }');
        if(res && res.data){
            const option_format = res.data.data.map(item => ( {value: item.activity_description, label: item.activity_description, impact : item.impact, risk : item.risk, change_category : item.change_category} ))
            setOptionActivityList(option_format)
        }
    }

    React.useEffect(() => {
        if(props.dataForm){
            setDataForm({
                ...dataForm,
                ...props.dataForm
            })
        }
    }, [props.dataForm])

    React.useEffect(() => {
        if(dataForm.current_status === undefined || dataForm.current_status === null || CR_CURRENT_STATUS_CAN_EDIT.includes(dataForm.current_status)){
            if(dataForm.oc_tier_2){
                getDataActivityList()
            }
        }
    }, [dataForm.oc_tier_2]);

    const downloadMOPFile = async (e) => {
        // e.preventDefault()
        const resFile = await apiAuth.getFile('/downloadTemplate/MOP');
        // console.log("resFile", resFile)
        if(resFile !== undefined && resFile.data){
            saveAs(new Blob([resFile.data], {type:resFile.data.type}), "Template MOP All Domain.docx");
        }
    }

    const downloadFile = async (e, file, idx) => {
        e.preventDefault()
        if(file !== undefined)  {
          const resFile = await apiAuth.getFile('/downloadAttachment/' + dataForm.change_id+'/'+idx);
          if(resFile !== undefined){
            saveAs(new Blob([resFile.data], {type:file.mime_type}), file.file_name);
          }
        }
      }

    let dataAttachment_0 = {}
    let dataAttachment_1 = {}
    let dataAttachment_2 = {}
    
    if(dataForm.attachments){
        dataAttachment_0 = dataForm.attachments.find(item => item.attachment_name === "Method of Procedure")
        dataAttachment_1 = dataForm.attachments.find(item => item.attachment_name === "Evidence")
        dataAttachment_2 = dataForm.attachments.find(item => item.attachment_name === "Supporting Attachment")
    }
        
    if(!dataAttachment_0){
        dataAttachment_0 = {}
    }
    if(!dataAttachment_1){
        dataAttachment_1 = {}
    }
    if(!dataAttachment_2){
        dataAttachment_2 = {}
    }

    // console.log("data Sub", dataForm.attachments)

    return (
        <React.Fragment>
            <Row>
                <Col md={6}>
                    <div className="mb-3">
                        <Label htmlFor="changeType" className="form-label">Activity List</Label>
                        <Select
                            value={dataForm && ({value : dataForm.activity_list, label : dataForm.activity_list})}
                            onChange={(e) => {
                                handleSelectForm(e, 'activity_list');
                            }}
                            options={optionActivityList}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col md={6}>
                    <div className="mb-3">
                        <Label htmlFor="changeCategory" className="form-label">Change Category</Label>
                        <Input type="text" className="form-control" name="change_category" disabled value={dataForm.change_category} onChange={(e) => handleInputChange(e)} id="descriptionOfChange" />
                    </div>
                </Col>
                <Col md={3}>
                    <div className="mb-3">
                        <Label htmlFor="impact" className="form-label">Impact</Label>
                        <Input type="text" className="form-control" name="impact" value={dataForm.impact} onChange={(e) => handleInputChange(e)} id="impact" disabled/>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="mb-3">
                        <Label htmlFor="risk" className="form-label">Risk</Label>
                        <Input type="text" className="form-control" name="risk" value={dataForm.risk} onChange={(e) => handleInputChange(e)} id="risk" disabled/>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col md={6}>
                    <div className="mb-3">
                        <Label htmlFor="priority" className="form-label">Priority</Label>
                        <Input type="text" className="form-control" name="priority" value={dataForm.priority} onChange={(e) => handleInputChange(e)} id="priority" disabled/>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col md={6}>
                    <div className="mb-3">
                        <Label htmlFor="descriptionOfChange" className="form-label">Description Of Change</Label>
                        <Input type="textarea" className="form-control" name="description_of_change" value={dataForm.description_of_change} onChange={(e) => handleInputChange(e)} id="descriptionOfChange" />
                    </div>
                </Col>
            </Row>
            <Row style={{borderBottom: "1px solid"}} >
                <Col md={6}>
                    <div className="mb-3">
                        <Label htmlFor="reasonForChange" className="form-label">Reason For Change</Label>
                        <Input type="textarea" className="form-control" name="reason_for_change" value={dataForm.reason_for_change} onChange={(e) => handleInputChange(e)} id="reasonForChange" />
                    </div>
                </Col>
            </Row>
            
            <Row key={0} style={{paddingTop: "10px", border: "1px solid", borderTop: "0px", borderBottom: "0px"}}>
                <Col md={4}>
                    <div className="mb-3">
                        <Label htmlFor="attachmentName" className="form-label">Attachment Name
                            
                        </Label>
                        <Input type="text" className="form-control" name="attachment_name" style={{backgroundColor: "#ffffff"}} disabled value={dataAddSub[0].attachment_name !== null ? dataAddSub[0].attachment_name : ''} id="attachment_name" />
                    </div>
                </Col>
                <Col md={2}>
                    <Label htmlFor="donwload_file" className="form-label">Download File</Label>
                    {dataAttachment_0.attachment_name && (
                        <React.Fragment>
                            <Button color='success' onClick={(e) => {downloadFile(e, dataAttachment_0, 0)}}>
                                Download
                            </Button>
                        </React.Fragment>
                    )}
                </Col>
                <Col md={4}>
                    <div className="mb-3">
                        <Label htmlFor="file" className="form-label">Upload File
                            <div style={{marginLeft: "5px"}} className={"icon-clasification-attachment"}> <i className={"ri-information-line"}></i> <span class="icon-text-clasification-attachment">Methode of Precedure<br/>
                                Berisi dokumen step by step activity, PIC, scenario fallback, dll. Anda dapat download template MOP disamping.</span>
                            </div>
                        </Label>
                        <Input
                            type="file"
                            style={{marginBottom: "10px"}}
                            accept=".doc, .docx, .xlsx, .xls, .pdf, .jpg, .png, .gif, .txt, .zip, .rar"
                            name={0}
                            onChange={(e) => {
                                handleInputFileForm(e, "file", 0);
                            }}
                        />
                        <span style={{color: "#707070"}}><b>File Type</b> : docx, xls, xlsx, pdf, jpg, png, gif, txt, zip, rar</span>
                    </div>
                </Col>
                <Col md={2} className="mb-3">
                    <Label htmlFor="size" className="form-label">Download Template</Label>
                    <Button className="form-control" name="download_template" color="light" style={{border: "1px solid #dedede"}} onClick={(e) => {downloadMOPFile(e)}}><i className={"ri-download-2-fill"}></i> MOP Template</Button>
                </Col>
            </Row>
            <Row key={1} style={{paddingTop: "10px", border: "1px solid", borderTop: "1px solid #dedede", borderBottom: "0px"}}>
                <Col md={4}>
                    <div className="mb-3">
                        <Label htmlFor="attachmentName" className="form-label">Attachment Name</Label>
                        <Input type="text" className="form-control" name="attachment_name" style={{backgroundColor: "#ffffff"}} disabled value={dataAddSub[1].attachment_name !== null ? dataAddSub[1].attachment_name : ''} id="attachment_name" />
                    </div>
                </Col>
                <Col md={2}>
                    <Label htmlFor="donwload_file" className="form-label">Download File</Label>
                    {dataAttachment_1.attachment_name && (
                        <React.Fragment>
                            <Button color='success' onClick={(e) => {downloadFile(e, dataAttachment_1, 1)}}>
                                Download
                            </Button>
                        </React.Fragment>
                    )}
                </Col>
                <Col md={4}>
                    <div className="mb-3">
                        <Label htmlFor="file" className="form-label">Upload File
                            <div style={{marginLeft: "5px"}} className={"icon-clasification-attachment"}><i className={"ri-information-line"}></i> <span class="icon-text-clasification-attachment">Evidence<br/>
                                Berisi dokumen evidence sebagai dasar permohonan activity seperti Nota Dinas, MOM, Email, dll.</span>
                            </div>
                        </Label>
                        <Input
                            type="file"
                            style={{marginBottom: "10px"}}
                            accept=".doc, .docx, .xlsx, .xls, .pdf, .jpg, .png, .gif, .txt, .zip, .rar"
                            name={1}
                            onChange={(e) => {
                                handleInputFileForm(e, "file", 1);
                            }}
                        />
                        <span style={{color: "#707070"}}><b>File Type</b> : docx, xls, xlsx, pdf, jpg, png, gif, txt, zip, rar</span>
                    </div>
                </Col>
                <Col md={2}>
                    <div className="mb-3">
                        <Label htmlFor="size" className="form-label"></Label>
                    </div>
                </Col>
            </Row>
            <Row key={2} style={{paddingTop: "10px", border: "1px solid", borderTop: "1px solid #dedede", borderBottom: "1px solid"}}>
                <Col md={4}>
                    <div className="mb-3">
                        <Label htmlFor="attachmentName" className="form-label">Attachment Name</Label>
                        <Input type="text" className="form-control" name="attachment_name" style={{backgroundColor: "#ffffff"}} disabled value={dataAddSub[2].attachment_name !== null ? dataAddSub[2].attachment_name : ''} id="attachment_name" />
                    </div>
                </Col>
                <Col md={2}>
                    <Label htmlFor="donwload_file" className="form-label">Download File</Label>
                    {dataAttachment_2.attachment_name && (
                        <React.Fragment>
                            <Button color='success' onClick={(e) => {downloadFile(e, dataAttachment_2, 2)}}>
                                Download
                            </Button>
                        </React.Fragment>
                    )}
                    
                </Col>
                <Col md={4}>
                    <div className="mb-3">
                        <Label htmlFor="file" className="form-label">Upload File
                            <div style={{marginLeft: "5px"}} className={"icon-clasification-attachment"}><i className={"ri-information-line"}></i> <span class="icon-text-clasification-attachment">Supporting Attachment<br/>
                                Berisi dokumen pendukung kelengkapan activity seperti script, timeplan, dll.</span>
                            </div>
                        </Label>
                        <Input
                            type="file"
                            name={2}
                            onChange={(e) => {
                                handleInputFileForm(e, "file", 2);
                            }}
                        />
                    </div>
                </Col>
                <Col md={2}>
                    <div className="mb-3">
                        <Label htmlFor="size" className="form-label"></Label>
                    </div>
                </Col>
            </Row>
                
        </React.Fragment>
    );
};

export default CRUpdateClassification;