import React from 'react';
import { Card, CardHeader, CardBody, Col, Container, Form, Input, Label, Row } from 'reactstrap';
import Select from "react-select";

const GroupSelectedForm = [
    { value: 'Zero', label: 'Zero' },
    { value: 'Two', label: 'Two' },
    { value: 'Four', label: 'Four' },
    { value: 'One', label: 'One' },
    { value: 'Five', label: 'Five' },
    { value: 'Three', label: 'Three' },
    { value: 'Six', label: 'Six' },
];

const GroupSelectedForm2 = [
    { value: 'Zero', label: 'Zero' },
    { value: 'Two', label: 'Two' },
    { value: 'Four', label: 'Four' },
    { value: 'One', label: 'One' },
    { value: 'Five', label: 'Five' },
    { value: 'Three', label: 'Three' },
    { value: 'Six', label: 'Six' },
];

const GroupSelectedForm3 = [
    { value: 'Zero', label: 'Zero' },
    { value: 'Two', label: 'Two' },
    { value: 'Four', label: 'Four' },
    { value: 'One', label: 'One' },
    { value: 'Five', label: 'Five' },
    { value: 'Three', label: 'Three' },
    { value: 'Six', label: 'Six' },
];

const GroupSelectedForm4 = [
    { value: 'Zero', label: 'Zero' },
    { value: 'Two', label: 'Two' },
    { value: 'Four', label: 'Four' },
    { value: 'One', label: 'One' },
    { value: 'Five', label: 'Five' },
    { value: 'Three', label: 'Three' },
    { value: 'Six', label: 'Six' },
];

const GroupSelectedForm5 = [
    { value: 'Zero', label: 'Zero' },
    { value: 'Two', label: 'Two' },
    { value: 'Four', label: 'Four' },
    { value: 'One', label: 'One' },
    { value: 'Five', label: 'Five' },
    { value: 'Three', label: 'Three' },
    { value: 'Six', label: 'Six' },
];

const CRFormRequestor = (props) => {

    const [dataForm, setDataForm] = React.useState({});

    const handleInputChange = (e) => {
        const dataFormInput = {...dataForm}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataForm(dataFormInput)
    }

    React.useEffect(() => {
        if(props.dataForm){
            setDataForm({...props.dataForm})
        }
    }, [props.dataForm])

    return (
        <React.Fragment>
            <Row>
                <Col md={4}>
                    <div className="mb-3">
                        <Label htmlFor="first_name" className="form-label">First Name</Label>
                        <Input type="text" className="form-control" name="first_name" value={dataForm.first_name} disabled onChange={(e) => handleInputChange(e)} id="first_name" />
                    </div>
                    <div className="mb-3">
                        <Label htmlFor="middle_initial" className="form-label">Middle Name</Label>
                        <Input type="text" className="form-control" name="middle_initial" value={dataForm.middle_initial} disabled onChange={(e) => handleInputChange(e)} id="middle_initial" />
                    </div>
                    <div className="mb-3">
                        <Label htmlFor="last_name" className="form-label">Last Name</Label>
                        <Input type="text" className="form-control" name="last_name" value={dataForm.last_name} disabled onChange={(e) => handleInputChange(e)} id="last_name" />
                    </div>
                    <div className="mb-3">
                        <Label htmlFor="phone_number" className="form-label">Phone Number</Label>
                        <Input type="text" className="form-control" name="phone_number" value={dataForm.phone_number} disabled onChange={(e) => handleInputChange(e)} id="phone_number" />
                    </div>
                    <div className="mb-3">
                        <Label htmlFor="email" className="form-label">Email</Label>
                        <Input type="text" className="form-control" name="email" value={dataForm.email} disabled onChange={(e) => handleInputChange(e)} id="email" />
                    </div>
                </Col>
                <Col md={4}>
                    <div className="mb-3">
                        <Label htmlFor="organization_long" className="form-label">Organization Long</Label>
                        <Input type="text" className="form-control" name="organization_long" disabled value={dataForm.organization_long} onChange={(e) => handleInputChange(e)} id="organization_long" />
                    </div>
                    <div className="mb-3">
                        <Label htmlFor="department_long" className="form-label">Department Long</Label>
                        <Input type="text" className="form-control" name="department_long" disabled value={dataForm.department_long} onChange={(e) => handleInputChange(e)} id="department_long" />
                    </div>
                </Col>
                <Col md={4}>
                    <div className="mb-3">
                        <Label htmlFor="support_organization" className="form-label">Support Organization</Label>
                        <Input type="text" className="form-control" name="support_organization" disabled value={dataForm.first_name} onChange={(e) => handleInputChange(e)} id="support_organization" />
                    </div>
                    <div className="mb-3">
                        <Label htmlFor="support_group_name" className="form-label">Support Group Name</Label>
                        <Input type="text" className="form-control" name="support_group_name" disabled value={dataForm.support_group_name} onChange={(e) => handleInputChange(e)} id="support_group_name" />
                    </div>
                </Col>
            </Row>
        </React.Fragment>
    );
};

export default CRFormRequestor;