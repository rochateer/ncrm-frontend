import React from 'react';
import { Card, CardHeader, CardBody, Col, Container, Form, Input, Label, Row } from 'reactstrap';
import Select from "react-select";

const GroupSelectedForm = [
    { value: 'Zero', label: 'Zero' },
    { value: 'Two', label: 'Two' },
    { value: 'Four', label: 'Four' },
    { value: 'One', label: 'One' },
    { value: 'Five', label: 'Five' },
    { value: 'Three', label: 'Three' },
    { value: 'Six', label: 'Six' },
];

const GroupSelectedForm2 = [
    { value: 'Zero', label: 'Zero' },
    { value: 'Two', label: 'Two' },
    { value: 'Four', label: 'Four' },
    { value: 'One', label: 'One' },
    { value: 'Five', label: 'Five' },
    { value: 'Three', label: 'Three' },
    { value: 'Six', label: 'Six' },
];

const GroupSelectedForm3 = [
    { value: 'Zero', label: 'Zero' },
    { value: 'Two', label: 'Two' },
    { value: 'Four', label: 'Four' },
    { value: 'One', label: 'One' },
    { value: 'Five', label: 'Five' },
    { value: 'Three', label: 'Three' },
    { value: 'Six', label: 'Six' },
];

const CRFormGeneral = () => {

    const [dataForm, setDataForm] = React.useState({});
    const [selectedForm, setSelectedForm] = React.useState(null);
    const [selectedForm2, setSelectedForm2] = React.useState(null);
    const [selectedForm3, setSelectedForm3] = React.useState(null);

    const handleInputChange = (e) => {
        const dataFormInput = {...dataForm}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataForm(dataFormInput)
    }

    function handleSelectForm(selectedForm) {
        setSelectedForm(selectedForm);
    }

    function handleSelectForm2(selectedForm2) {
        setSelectedForm2(selectedForm2);
    }

    function handleSelectForm3(selectedForm3) {
        setSelectedForm3(selectedForm3);
    }

    return (
        <React.Fragment>
            <Row>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="changeID" className="form-label">Change ID</Label>
                        <Input type="text" className="form-control" name="changeid" onChange={(e) => handleInputChange(e)} id="changeID" />
                    </div>
                </Col>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="coordinatorGroup" className="form-label">Coordinator Group</Label>
                        <Select
                            isClearable={true}
                            value={selectedForm}
                            onChange={() => {
                                handleSelectForm();
                            }}
                            options={GroupSelectedForm}
                        />
                    </div>
                </Col>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="changeCoordinator" className="form-label">Change Coordinator</Label>
                        <Select
                            isClearable={true}
                            value={selectedForm2}
                            onChange={() => {
                                handleSelectForm2();
                            }}
                            options={GroupSelectedForm2}
                        />
                    </div>
                </Col>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="changeLocation" className="form-label">Change Location</Label>
                        <Input type="text" className="form-control" name="changelocation" onChange={(e) => handleInputChange(e)} id="changeLocation" />
                    </div>
                </Col>
            </Row>
            <Row>
            <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="service" className="form-label">Service</Label>
                        <Select
                            isClearable={true}
                            value={selectedForm3}
                            onChange={() => {
                                handleSelectForm3();
                            }}
                            options={GroupSelectedForm3}
                        />
                    </div>
                </Col>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="template" className="form-label">Template</Label>
                        <Input type="text" className="form-control" name="template" onChange={(e) => handleInputChange(e)} id="template" />
                    </div>
                </Col>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="summary" className="form-label">Summary</Label>
                        <Input type="text" className="form-control" name="summary" onChange={(e) => handleInputChange(e)} id="summary" />
                    </div>
                </Col>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="description" className="form-label">Description</Label>
                        <Input type="textarea" className="form-control" name="description" onChange={(e) => handleInputChange(e)} id="description" />
                    </div>
                </Col>
            </Row>
        </React.Fragment>
    );
};

export default CRFormGeneral;