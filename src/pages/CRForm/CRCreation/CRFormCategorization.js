import React from 'react';
import { Card, CardHeader, CardBody, Col, Container, Form, Input, Label, Row } from 'reactstrap';
import Select from "react-select";

import { API_CLIENT_AUTH_BEARER } from '../../../helpers/api-helper';
import { CR_CURRENT_STATUS_CAN_EDIT } from '../../../config/data/CR_CURRENT_STATUS_CAN_EDIT';

const apiAuth = new API_CLIENT_AUTH_BEARER();

const groupSelectFormCompanyLocation = [
    { value: 'PT Telekomunikasi Selular', label: 'PT Telekomunikasi Selular' },
    { value: 'PT Telkom Indonesia', label: 'PT Telkom Indonesia' },    
];

const CRFormCategorization = (props) => {

    const [optionTier1, setOptionTier1] = React.useState([]);
    const [optionDomain, setOptionDomain] = React.useState([]);
    const [optionTier2, setOptionTier2] = React.useState([]);
    const [optionTier3, setOptionTier3] = React.useState([]);

    const Activity_List_Select_Option = props.activityList;

    const [dataForm, setDataForm] = React.useState({oc_tier_1 : "Change & Release Management"});
    const [requiredCatgorization, setRequiredCatgorization] = React.useState([false, true, false, false, false]);

    const handleSelectForm = (e, name, idxFlag) => {
        const data_form = {...dataForm};
        const {value, label, priority} = e;
        data_form[name] = value;
        if(name === "oc_tier_3"){
            data_form["priority"] = priority;
            props.onChangeFormAll(data_form);
        }else{
            props.onChangeForm(name, value);
        }

        if(value !== null && value !== ""){
            requiredCatgorization[idxFlag] = true
        }else{
            requiredCatgorization[idxFlag] = false
        }
        props.onChangeRequiredCategorization(requiredCatgorization);

        setDataForm(data_form);
    }

    const getDataTier1 = async() => {
        const res = await apiAuth.get('/getActivityListTier1');
        if(res && res.data){
            const option_format = res.data.data.filter(item => item !== "Work Order").map(item => ( {value: item, label: item} ))
            setOptionTier1(option_format)
        }
    }

    const getDataDomainCategory = async() => {
        const reqBody = {
            "data": {
                "categorization_tier_1": dataForm.oc_tier_1
            }
        };
        const res = await apiAuth.post('/getActivityListDomainCategory', reqBody);
        if(res && res.data){
            const option_format = res.data.data.map(item => ( {value: item, label: item} ))
            setOptionDomain(option_format)
        }
    }

    const getDataTier2 = async() => {
        const reqBody = {
            "data": {
                "categorization_tier_1": dataForm.oc_tier_1,
                "domain_category": dataForm.oc_domain_category
            }
        };
        const res = await apiAuth.post('/getActivityListTier2', reqBody);
        if(res && res.data){
            const option_format = res.data.data.map(item => ( {value: item, label: item} ))
            setOptionTier2(option_format)
        }
    }

    const getDataTier3 = async() => {
        const option_format = [
            {label : "Normal", value : "Normal", priority : "Normal"},
            {label : "Expedited", value : "Expedited", priority : "Urgent"},
            {label : "Incident", value : "Incident", priority : "Urgent"},
        ]
        setOptionTier3(option_format)
    }

    React.useEffect(() => {
        if(props.dataForm){
            setDataForm({
                ...dataForm,
                ...props.dataForm
            })
        }
    }, [props.dataForm])

    React.useEffect(() => {
        if(dataForm.current_status === undefined || dataForm.current_status === null || CR_CURRENT_STATUS_CAN_EDIT.includes(dataForm.current_status)){
            getDataTier1()
        }
    }, []);

    React.useEffect(() => {
        if(dataForm.current_status === undefined || dataForm.current_status === null || CR_CURRENT_STATUS_CAN_EDIT.includes(dataForm.current_status)){
            getDataDomainCategory()
        }
    }, [dataForm.oc_tier_1])

    React.useEffect(() => {
        if(dataForm.current_status === undefined || dataForm.current_status === null || CR_CURRENT_STATUS_CAN_EDIT.includes(dataForm.current_status)){
            if(dataForm.oc_domain_category){
                getDataTier2()
            }
        }
    }, [dataForm.oc_domain_category])

    React.useEffect(() => {
        if(dataForm.current_status === undefined || dataForm.current_status === null || CR_CURRENT_STATUS_CAN_EDIT.includes(dataForm.current_status)){
            if(dataForm.oc_tier_2){
                getDataTier3()
            }
        }
    }, [dataForm.oc_tier_2])

    return (
        <React.Fragment>
            <Row style={{paddingTop: "10px"}}>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="changeLocationCompany" className="form-label">Change Location Company <span className="text-danger">*</span></Label>
                        <Select
                            value={dataForm && ({value : dataForm.oc_change_location_company, label : dataForm.oc_change_location_company})}
                            onChange={(e) => {
                                handleSelectForm(e, 'oc_change_location_company', 0);
                            }}
                            options={groupSelectFormCompanyLocation}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                    <div className="mb-3">
                        <h5>Operational Categorization <span className="text-danger">*</span></h5>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="tier1" className="form-label">Tier 1 <span className="text-danger">*</span></Label>
                        <Select
                            value={dataForm.oc_tier_1 && ({value : dataForm.oc_tier_1, label : dataForm.oc_tier_1})}
                            onChange={(e) => {
                                handleSelectForm(e, 'oc_tier_1', 1);
                            }}
                            options={optionTier1}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="domainCategory" className="form-label">Domain Category <span className="text-danger">*</span></Label>
                        <Select
                            value={dataForm && ({value : dataForm.oc_domain_category, label : dataForm.oc_domain_category})}
                            onChange={(e) => {
                                handleSelectForm(e, 'oc_domain_category', 2);
                            }}
                            options={optionDomain}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="tier2" className="form-label">Tier 2 <span className="text-danger">*</span></Label>
                        <Select
                            value={dataForm && ({value : dataForm.oc_tier_2, label : dataForm.oc_tier_2})}
                            onChange={(e) => {
                                handleSelectForm(e, 'oc_tier_2', 3);
                            }}
                            options={optionTier2}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="tier3" className="form-label">Tier 3 <span className="text-danger">*</span></Label>
                        <Select
                            value={dataForm && ({value : dataForm.oc_tier_3, label : dataForm.oc_tier_3})}
                            onChange={(e) => {
                                handleSelectForm(e, 'oc_tier_3', 4);
                            }}
                            options={optionTier3}
                        />
                    </div>
                </Col>
            </Row>
        </React.Fragment>
    );
};

export default CRFormCategorization;