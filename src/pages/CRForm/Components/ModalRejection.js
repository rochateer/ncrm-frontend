import React from 'react'

import { Card, CardHeader, CardBody, Col, Container, Row, Collapse, Input, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
// import { API_CLIENT_AUTH_BEARER } from '../../helpers/api-helper';
import classnames from "classnames";

// const api = new APIClient();
// const apiAuth = new API_CLIENT_AUTH_BEARER();
  
export default function ModalRejection(props) {

  const [dropdownFilter, setDropdownFilter] = React.useState(false);
  const [dataFilter, setDataFilter] = React.useState({});

  const openFilter = (e) => {
    setDropdownFilter(!dropdownFilter);
  }

  const handleFilterInputan = (e) => {
    const data_filter = {...dataFilter};
    const {value, name} = e.target;
    data_filter[name] = value;
    setDataFilter(data_filter);
  }

  const deleteFilterInputan = (e) => {
    const data_filter = {...dataFilter};
    for (const field_name in data_filter) {
      data_filter[field_name] = ''
    }
    setDataFilter(data_filter);
  }

  const getDataFilter = () => {
    props.doFilter(dataFilter)
  }

  return (
    <React.Fragment>
      <Modal isOpen={modalReject} toggle={toggleReject} style={{marginTop : '200px'}}>
            <ModalHeader toggle={toggleReject}>Reject {data.change_id}</ModalHeader>
            <ModalBody>
            <Row>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="reject_remark" className="form-label">Reject Remarks</Label>
                        <Input type="textarea" className="form-control" value={dataApproval.status_note} name="status_note" onChange={(e) => handleInputApprovalDataChange(e)} id="status_note" />
                    </div>
                </Col>
            </Row>
            </ModalBody>
            <ModalFooter>
            <Button color="danger" onClick={() => approveCrForm(roleRejection, "Rejected")} >
                Reject
            </Button>{' '}
            <Button color="secondary" onClick={toggleReject}>
                Cancel
            </Button>
            </ModalFooter>
        </Modal>
    </React.Fragment>
  )
}
