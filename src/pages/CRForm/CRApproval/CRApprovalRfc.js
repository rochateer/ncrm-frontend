import React from 'react';

import Select from 'react-select';

import { Card, CardHeader, CardBody, Col, Container, Form, Input, Label, Row, Button, Spinner, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';

import { dateformatncrm } from '../../../helpers/FunctionalHelper/date-helper';

import { API_CLIENT_AUTH_BEARER } from "../../../helpers/api-helper";
import PaginationTable from '../../../Components/TableComponent/PaginationTable';
import { LIST_CAB_FORM_QUESTION } from './common/LIST_CAB_FORM_QUESTION';
import GeneralDataTab from '../CRDetail/GeneralDataTab';

const apiAuth = new API_CLIENT_AUTH_BEARER();

const CRApprovalRfc = (props) => {

    const {data, USER_PROFILE, actionLoading} = props;

    const [loadingStatus, setLoadingStatus] = React.useState(false);

    const [dataApproval, setDataApproval] = React.useState({"status_note" : ""});

    const [listUserApproval, setListUserApproval] = React.useState([]);

    const [cabMom, setCabMom] = React.useState('test cab');

    const [cabForm, setCabForm] = React.useState([...LIST_CAB_FORM_QUESTION]);

    const [showAddApproval, setShowAddApproval] = React.useState(false);

    const LIST_CAB_FORM_PARENT = LIST_CAB_FORM_QUESTION.filter(item => item.no === item.parent_no);

    const [dataUser, setDataUser] = React.useState([]);
    const [pageUser, setPageUser] = React.useState(1);
    const [totalDataUser, setTotalDataUser] = React.useState(0);
    const [perPageUser, setPerPageUser] = React.useState(25);

    const [dataForm, setDataForm] = React.useState({});
    const [canEdit, setCanEdit] = React.useState(false);

    const [modalReject, setModalReject] = React.useState(false);
    const [roleRejection, setRoleRejection] = React.useState(null);

    const toggleReject = (approval_role) => {
        if(approval_role){
            setRoleRejection(approval_role)
        }else{
            setRoleRejection(null)
        }
        setModalReject(!modalReject)
    };

    const handleInputApprovalDataChange = (e) => {
        const dataFormInput = {...dataApproval}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataApproval(dataFormInput)
    }

    const onChangePageUser = (e) => {
        setPageUser(e);
      }

    React.useEffect(() => {
        getDataUser()
    }, []);

    const getDataUser = async() => {
        const res = await apiAuth.get('/getUser?lmt='+perPageUser+'&pg='+pageUser);
        if(res && res.data){
            setDataUser(res.data.data)
            setTotalDataUser(res.data.totalResults)
        }
    }

    const handleInputChange = (e) => {
        const dataFormInput = {...dataForm}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataForm(dataFormInput)
    }

    const addApproval = () => {
        let list_approval = [...listUserApproval];
        list_approval.push({})
        setListUserApproval(list_approval)
    }

    const approveCrForm = (approval_role, status) => {
        if(modalReject === true){
            toggleReject()
        }
        let data_approval = {...dataApproval}
        data_approval["role"] = approval_role;
        data_approval["status"] = status;
        if((data.change_category === "Normal Major" || data.change_category === "Normal Significant")){
            if(status === "Approved"){
                data_approval["cab_form"] = cabForm;
            }else{
                data_approval["cab_form"] = data.cab_forms ? data.cab_forms : []
            }
            if(data.responsible_approval){
                data_approval["approval_sfa"] = data.responsible_approval.map(item => ({ "role" : item }));
                data_approval["approval_sfa"] = data_approval["approval_sfa"].concat([{ "role" : "Expert Domain" }, { "role" : "Change Manager" }]);
            }
        }
        if(status === "Approved"){
            data_approval["status_note"] = "Approved";
        }
        props.actionApproval(data_approval);
    }

    const handleInputMoMChange = (e) => {
        let cab_mom = cabMom
        const {name, value} = e.target
        cab_mom = value
        setCabMom(cab_mom)
    } 

    const handleCheckboxUserChange = (e, id, value) => {

        let list_approval = [...listUserApproval]

        const {checked} = e.target;

        if(checked){
            const data_checked = dataUser.find(du => du.email === id);
            list_approval.push(data_checked)
        }else{
            list_approval = list_approval.filter(du => du.email !== id )
        }

        setListUserApproval(list_approval);
    }

    const handleInputCabForm = (e, pos, type) => {

        const form_cab = [...cabForm];

        const {value, name} = e.target;

        if(type === 'child'){
            const pos_child = pos.split("#")
            const no_parent = pos_child[0]
            const no_child = pos_child[1]
            const idx_form = form_cab.findIndex(item => item.parent_no === parseInt(no_parent) && item.no === parseInt(no_child) );
            form_cab[idx_form][name] = value
        }else{
            const idx_form = form_cab.findIndex(item => item.no === pos);
            form_cab[idx_form][name] = value;
        }

        setCabForm(form_cab);

    }

    const handleCloseCrApproval = () => setShowAddApproval(false);
    const onClickAddCrApproval = () => setShowAddApproval(true);

    return (
        <React.Fragment>
            <Row>
                <Col xl={12}>
                    <Card>
                        <CardBody>
                            <React.Fragment>
                                <Row >
                                <Col xl={12}>
                                <div className="text-center">
                                    <h4>CAB FORM</h4>
                                </div>
                                </Col>
                                <Col xl={12}>
                                    <Card>
                                    <CardBody>
                                    {((data.change_category === "Normal Major" || data.change_category === "Normal Significant") && (data.current_status === "Request for Change")) && (
                                    <div className="table-responsive table-card">
                                        <table className="table align-middle table-striped-columns mb-0">
                                            <thead className="table-light">
                                                <tr>
                                                    <th scope='col'>No</th>
                                                    <th scope="col">Question</th>
                                                    <th scope="col" style={{width: "100px"}}>Yes / No</th>
                                                    <th scope="col" style={{width: "350px"}}>Remark</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {LIST_CAB_FORM_PARENT.map((form, idx) =>
                                                <React.Fragment>
                                                    <tr key={idx}>
                                                        <td>{form.parent_no}</td>
                                                        <td>{form.question}</td>
                                                        <td>
                                                            {form.yes_or_no_input !== false && (
                                                            <select className="form-select" id="parent_yes_or_no" onChange={e => handleInputCabForm(e, form.parent_no)} name="yes_or_no">
                                                                <option ></option>
                                                                <option value="yes">Yes</option>
                                                                <option value="no">No</option>
                                                            </select>
                                                            )}
                                                        </td>
                                                        <td>
                                                            {form.remark_input !== false && (
                                                                <Input type="textarea" disabled={form.disabled_remark_if_y_n && (form.disabled_remark_if_y_n === cabForm.find(item => item.no === form.no)?.yes_or_no)} className="form-control" name="remarks" value={dataForm.remarks} id="remarks" onChange={e => handleInputCabForm(e, form.parent_no)}/>
                                                            )}
                                                        </td>
                                                    </tr>
                                                    {LIST_CAB_FORM_QUESTION.filter(item => item.parent_no === form.parent_no && item.no !== form.parent_no).map((form_child, idx_child) =>
                                                        <React.Fragment>
                                                            <tr key={idx+'#'+idx_child}>
                                                                <td></td>
                                                                <td>{form_child.question}</td>
                                                                <td>
                                                                    {form_child.yes_or_no_input !== false && (
                                                                        <select className="form-select" id="child_yes_or_no" disabled={cabForm.find(item => item.no === form.no)?.yes_or_no !== "yes"} onChange={e => handleInputCabForm(e, form.parent_no+'#'+form_child.no, 'child')} name="yes_or_no">
                                                                            <option ></option>
                                                                            <option defaultValue="yes">Yes</option>
                                                                            <option defaultValue="no ">No</option>
                                                                        </select>
                                                                    )}
                                                                </td>
                                                                <td>
                                                                    {form_child.remark_input !== false && (
                                                                        <Input type="textarea" className="form-control" name="remarks" disabled={cabForm.find(item => item.no === form_child.parent_no)?.yes_or_no !== "yes"} value={dataForm.remarks} id="remarks" onChange={e => handleInputCabForm(e, form.parent_no+'#'+form_child.no, 'child')}/>
                                                                    )}
                                                                </td>
                                                            </tr>
                                                        </React.Fragment>
                                                    )}
                                                </React.Fragment>
                                                )}
                                            </tbody>
                                        </table>
                                    </div>
                                    )}
                                    </CardBody>
                                    </Card>
                                </Col>
                                {data.current_status === "Request for Change" && (
                                    <React.Fragment>
                                        <Col xl={12}>
                                            <div className="text-center">
                                                <Row>
                                                    <Col xl={12}>
                                                        <Row>
                                                            <Col xs={12} >
                                                                <h4>Change Coordinator</h4>
                                                            </Col>
                                                        </Row>
                                                        <Row >
                                                            <Col xs={12}>
                                                                {(USER_PROFILE && USER_PROFILE.role && (USER_PROFILE.role.find(rl => rl === "Change Coordinator") || USER_PROFILE.role.find(rl => rl === "Admin"))) ? (
                                                                    <React.Fragment>
                                                                    <Button onClick={(e) => approveCrForm("Change Coordinator", "Approved")} disabled={actionLoading}>
                                                                        {actionLoading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                                        Submit
                                                                    </Button>
                                                                    <Button
                                                                        onClick={(e) => toggleReject("Change Coordinator")} 
                                                                        color='danger'
                                                                        style={{marginLeft : '100px'}}
                                                                        disabled={actionLoading}
                                                                        // disabled={loadingStatus}
                                                                    >
                                                                    {actionLoading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                                    Reject 
                                                                    </Button>
                                                                    </React.Fragment>
                                                                ) : (
                                                                    <h2>
                                                                        NOT YET
                                                                    </h2>
                                                                )}
                                                            </Col>
                                                        </Row>
                                                    
                                                    </Col>
                                                </Row>
                                            </div>
                                        </Col>
                                    </React.Fragment>
                                )}
                                
                            </Row>
                            </React.Fragment>
                        </CardBody>
                    </Card>
                </Col>
            </Row>

            <Row>
                <Col xl={12}>
                    <Card>
                        <CardBody>
                            <GeneralDataTab data={data}  onEdit={false}/>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
            
            <Modal isOpen={modalReject} toggle={toggleReject} style={{marginTop : '200px'}}>
                <ModalHeader toggle={toggleReject}>Reject {data.change_id}</ModalHeader>
                <ModalBody>
                <Row>
                    <Col md={12}>
                        <div className="mb-3">
                            <Label htmlFor="reject_remark" className="form-label">Reject Remarks</Label>
                            <Input type="textarea" className="form-control" value={dataApproval.status_note} name="status_note" onChange={(e) => handleInputApprovalDataChange(e)} id="status_note" />
                        </div>
                    </Col>
                </Row>
                </ModalBody>
                <ModalFooter>
                <Button color="danger" onClick={() => approveCrForm(roleRejection, "Rejected")} >
                    Reject
                </Button>{' '}
                <Button color="secondary" onClick={toggleReject}>
                    Cancel
                </Button>
                </ModalFooter>
            </Modal>

        </React.Fragment>
    );
};

export default CRApprovalRfc;