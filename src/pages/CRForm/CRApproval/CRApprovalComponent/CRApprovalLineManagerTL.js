import React from 'react';

import Select from 'react-select';

import { Card, CardHeader, CardBody, Col, Container, Form, Input, Label, Row, Button, Spinner, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import GeneralDataTab from '../../CRDetail/GeneralDataTab';

const CRApprovalLineManagerTL = (props) => {

    const {data, USER_PROFILE, actionLoading} = props;

    const [loadingStatus, setLoadingStatus] = React.useState(false)

    const [dataForm, setDataForm] = React.useState({});
    const [dataApproval, setDataApproval] = React.useState({"status_note" : ""});
    const [canEdit, setCanEdit] = React.useState(false);

    const [modalReject, setModalReject] = React.useState(false);

    const toggleReject = () => setModalReject(!modalReject);

    const handleInputApprovalDataChange = (e) => {
        const dataFormInput = {...dataApproval}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataApproval(dataFormInput)
    }

    const handleInputChange = (e) => {
        const dataFormInput = {...dataForm}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataForm(dataFormInput)
    }

    const approveCrForm = (status) => {
        if(modalReject === true){
            toggleReject()
        }
        let data_approval = {...dataApproval};
        data_approval["status"] = status;
        if(status === "Approved"){
            data_approval["status_note"] = "Approved";
        }
        // console.log("data_approval", data_approval)
        props.actionApproval(data_approval)
    }

    return (
        <React.Fragment>
            <Row>
                <Col xl={12}>
                    <Card style={{marginBottom : '10px'}}>
                        <CardBody>
                            {data.current_status === "Draft" && (
                                <React.Fragment>
                                    <div className="text-center">
                                        <Row>
                                            <Col xl={12}>
                                                <Row style={{maxWidth: "310px", marginLeft: "auto", marginRight: "auto", marginBottom : '10px'}} >
                                                    <Col xs={12}>
                                                        <h5>Line Manager Approval</h5>
                                                    </Col>
                                                </Row>
                                                <Row style={{maxWidth: "310px", marginLeft: "auto", marginRight: "auto"}}>
                                                    {/* <Col xs={12}> */}
                                                        {(USER_PROFILE && USER_PROFILE.role && (USER_PROFILE.role.find(rl => rl === "Admin") || (USER_PROFILE.email === data.lm_email))) ? (
                                                            <React.Fragment>
                                                                <Col md={6}>
                                                                    <Button 
                                                                        
                                                                        style={{width: "100%"}}
                                                                        onClick={() => approveCrForm("Approved")} 
                                                                        disabled={actionLoading}
                                                                        size='sm'
                                                                        // disabled={loadingStatus}
                                                                    >
                                                                    {actionLoading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                                    Approve
                                                                    </Button>
                                                                </Col>
                                                                <Col md={6}>
                                                                    <Button 
                                                                        onClick={toggleReject} 
                                                                        color='danger'
                                                                        
                                                                        style={{width: "100%"}}
                                                                        disabled={actionLoading}
                                                                        size='sm'
                                                                        // disabled={loadingStatus}
                                                                    >
                                                                    {actionLoading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                                        Reject 
                                                                    </Button>
                                                                </Col>
                                                            </React.Fragment>
                                                        ) : (
                                                            <React.Fragment>
                                                                <h2>
                                                                    {data.current_status === "Draft" ? "Not Yet" : sfaa.status }
                                                                </h2>
                                                            </React.Fragment>
                                                        )}
                                                        
                                                    {/* </Col> */}
                                                </Row>
                                            
                                            </Col>
                                        </Row>
                                    </div>
                                </React.Fragment>
                            )}
                        </CardBody>
                    </Card>
                </Col>
            </Row>

            <Modal isOpen={modalReject} toggle={toggleReject} style={{marginTop : '200px'}}>
                <ModalHeader toggle={toggleReject}>Reject {data.change_id}</ModalHeader>
                <ModalBody>
                <Row>
                    <Col md={12}>
                        <div className="mb-3">
                            <Label htmlFor="reject_remark" className="form-label">Reject Remarks</Label>
                            <Input type="textarea" className="form-control" value={dataApproval.status_note} name="status_note" onChange={(e) => handleInputApprovalDataChange(e)} id="status_note" />
                        </div>
                    </Col>
                </Row>
                </ModalBody>
                <ModalFooter>
                <Button color="danger" onClick={() => approveCrForm("Rejected")} >
                    Reject
                </Button>{' '}
                <Button color="secondary" onClick={toggleReject}>
                    Cancel
                </Button>
                </ModalFooter>
            </Modal>
        </React.Fragment>
    );
};

export default CRApprovalLineManagerTL;