import React from 'react';

import Select from 'react-select';

import { Card, CardHeader, CardBody, Col, Container, Form, Input, Label, Row, Button, Spinner, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';

import { dateformatncrm } from '../../../../helpers/FunctionalHelper/date-helper';

import { API_CLIENT_AUTH_BEARER } from "../../../../helpers/api-helper";
import { LIST_CAB_FORM_QUESTION } from '../common/LIST_CAB_FORM_QUESTION';

const apiAuth = new API_CLIENT_AUTH_BEARER();

const CRApprovalRfcTL = (props) => {

    const {data, USER_PROFILE, actionLoading} = props;

    const [loadingStatus, setLoadingStatus] = React.useState(false);

    const [dataApproval, setDataApproval] = React.useState({"status_note" : ""});

    const [listUserApproval, setListUserApproval] = React.useState([]);

    const [cabMom, setCabMom] = React.useState('test cab');

    const [cabForm, setCabForm] = React.useState([...LIST_CAB_FORM_QUESTION]);

    const [showAddApproval, setShowAddApproval] = React.useState(false);

    const LIST_CAB_FORM_PARENT = LIST_CAB_FORM_QUESTION.filter(item => item.no === item.parent_no);

    const [dataUser, setDataUser] = React.useState([]);
    const [pageUser, setPageUser] = React.useState(1);
    const [totalDataUser, setTotalDataUser] = React.useState(0);
    const [perPageUser, setPerPageUser] = React.useState(25);

    const [dataForm, setDataForm] = React.useState({});
    const [canEdit, setCanEdit] = React.useState(false);

    const [modalReject, setModalReject] = React.useState(false);
    const [roleRejection, setRoleRejection] = React.useState(null);

    const toggleReject = (approval_role) => {
        if(approval_role){
            setRoleRejection(approval_role)
        }else{
            setRoleRejection(null)
        }
        setModalReject(!modalReject)
    };

    const handleInputApprovalDataChange = (e) => {
        const dataFormInput = {...dataApproval}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataApproval(dataFormInput)
    }

    const onChangePageUser = (e) => {
        setPageUser(e);
      }

    React.useEffect(() => {
        getDataUser()
    }, []);

    const getDataUser = async() => {
        const res = await apiAuth.get('/getUser?lmt='+perPageUser+'&pg='+pageUser);
        if(res && res.data){
            setDataUser(res.data.data)
            setTotalDataUser(res.data.totalResults)
        }
    }

    const handleInputChange = (e) => {
        const dataFormInput = {...dataForm}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataForm(dataFormInput)
    }

    const addApproval = () => {
        let list_approval = [...listUserApproval];
        list_approval.push({})
        setListUserApproval(list_approval)
    }

    const approveCrForm = (approval_role, status) => {
        if(modalReject === true){
            toggleReject()
        }
        let data_approval = {...dataApproval}
        data_approval["role"] = approval_role;
        data_approval["status"] = status;
        if((data.change_category === "Normal Major" || data.change_category === "Normal Significant")){
            if(status === "Approved"){
                data_approval["cab_form"] = cabForm;
            }else{
                data_approval["cab_form"] = data.cab_forms ? data.cab_forms : []
            }
            if(data.responsible_approval){
                data_approval["approval_sfa"] = data.responsible_approval.map(item => ({ "role" : item }));
                data_approval["approval_sfa"] = data_approval["approval_sfa"].concat([{ "role" : "Expert Domain" }, { "role" : "Change Manager" }]);
            }
        }
        if(status === "Approved"){
            data_approval["status_note"] = "Approved";
        }
        props.actionApproval(data_approval);
    }

    const handleInputMoMChange = (e) => {
        let cab_mom = cabMom
        const {name, value} = e.target
        cab_mom = value
        setCabMom(cab_mom)
    } 

    const handleCheckboxUserChange = (e, id, value) => {

        let list_approval = [...listUserApproval]

        const {checked} = e.target;

        if(checked){
            const data_checked = dataUser.find(du => du.email === id);
            list_approval.push(data_checked)
        }else{
            list_approval = list_approval.filter(du => du.email !== id )
        }

        setListUserApproval(list_approval);
    }

    const handleInputCabForm = (e, pos, type) => {

        const form_cab = [...cabForm];

        const {value, name} = e.target;

        if(type === 'child'){
            const pos_child = pos.split("#")
            const no_parent = pos_child[0]
            const no_child = pos_child[1]
            const idx_form = form_cab.findIndex(item => item.parent_no === parseInt(no_parent) && item.no === parseInt(no_child) );
            form_cab[idx_form][name] = value
        }else{
            const idx_form = form_cab.findIndex(item => item.no === pos);
            form_cab[idx_form][name] = value;
        }

        setCabForm(form_cab);

    }

    const handleCloseCrApproval = () => setShowAddApproval(false);
    const onClickAddCrApproval = () => setShowAddApproval(true);

    return (
        <React.Fragment>
            <Row>
                <Col xl={12}>
                    <Card>
                        <CardBody>
                            <React.Fragment>
                                <span>
                                    Please Fill and Submit CAB Form
                                </span>
                            </React.Fragment>
                        </CardBody>
                    </Card>
                </Col>
            </Row>

        </React.Fragment>
    );
};

export default CRApprovalRfcTL;