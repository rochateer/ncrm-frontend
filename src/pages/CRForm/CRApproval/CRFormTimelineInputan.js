import React from 'react';

import { Card, CardHeader, CardBody, Col, Container, Form, Input, Label, Row } from 'reactstrap';
import { datetimeformatiso } from '../../../helpers/FunctionalHelper/date-helper';

const CRFormTimelineInputan = (props) => {

    const [dataForm, setDataForm] = React.useState({
        start_time : "23:00",
        end_time : "04:00"
    });

    const handleInputChange = (e) => {
        const dataFormInput = {...dataForm}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataForm(dataFormInput)
    }

    const submitTimeline = (e) => {
        e.preventDefault()
        const formTimeline = {...dataForm};
        formTimeline["plan_status"] = "Purposed"
        formTimeline["plan_start_time"] = datetimeformatiso(formTimeline.start_date, formTimeline.start_time)
        formTimeline["plan_end_time"] = datetimeformatiso(formTimeline.end_date, formTimeline.end_time)
        props.addTimeline(formTimeline)
    }

    return (
        <React.Fragment>
            <Row>
                <Card>
                <Col md={12} className='mb-3 mt-3'>
                    <h4>Add Timeline</h4>
                </Col>
                <Col md={10}>
                    <div className="mb-3">
                        <Label htmlFor="start_date" className="form-label">Plan Start Date</Label>
                        <Input type="date" className="form-control" name="start_date" value={dataForm.start_date} onChange={(e) => handleInputChange(e)} id="start_date" />
                    </div>
                </Col>
                <Col md={8}>
                    <div className="mb-3">
                        <Label htmlFor="start_time" className="form-label">Plan Start Time</Label>
                        <Input type="time" className="form-control" name="start_time" value={dataForm.start_time} onChange={(e) => handleInputChange(e)} id="start_time" />
                    </div>
                </Col>
                <Col md={10}>
                    <div className="mb-3">
                        <Label htmlFor="end_date" className="form-label">Plan End Date</Label>
                        <Input type="date" className="form-control" name="end_date" value={dataForm.end_date} onChange={(e) => handleInputChange(e)} id="end_date" />
                    </div>
                </Col>
                <Col md={8}>
                    <div className="mb-3">
                        <Label htmlFor="end_time" className="form-label">Plan End Time</Label>
                        <Input type="time" className="form-control" name="end_time" value={dataForm.end_time} onChange={(e) => handleInputChange(e)} id="end_time" />
                    </div>
                </Col>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="activity_description" className="form-label">Activity Description</Label>
                        <Input type="text" className="form-control" name="activity_description" onChange={(e) => handleInputChange(e)} id="activity_description" />
                    </div>
                </Col>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="number_of_impacted_ne" className="form-label">Number of Impacted NE</Label>
                        <Input type="number" className="form-control" name="number_of_impacted_ne" onChange={(e) => handleInputChange(e)} id="number_of_impacted_ne" />
                    </div>
                </Col>
                <Col md={12}>
                    <div className="mb-3">
                        <Label htmlFor="detailed_of_impacted_ne" className="form-label">Detailed of Impacted NE</Label>
                        <Input type="textarea" className="form-control" name="detailed_of_impacted_ne" onChange={(e) => handleInputChange(e)} id="detailed_of_impacted_ne" />
                    </div>
                </Col>
                <Col md={12} className='mb-3'>
                    <div className="text-end">
                        <button onClick={(e) => submitTimeline(e)} className="btn btn-primary">Add</button>
                    </div>
                </Col>
                </Card>
            </Row>
        </React.Fragment>
    );
};

export default CRFormTimelineInputan;