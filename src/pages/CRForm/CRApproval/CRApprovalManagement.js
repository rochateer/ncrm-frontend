import React from 'react';

import Select from 'react-select';

import { Card, CardHeader, CardBody, Col, Container, Form, Input, Label, Row, Button, Spinner, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { dateformatncrm, datetimeformatncrm } from '../../../helpers/FunctionalHelper/date-helper';
import GeneralDataTab from '../CRDetail/GeneralDataTab';

const CRApprovalManagement = (props) => {

    const {data, actionLoading, USER_PROFILE} = props;

    const [loadingStatus, setLoadingStatus] = React.useState(false)

    const [dataForm, setDataForm] = React.useState({});
    const [dataApproval, setDataApproval] = React.useState({"status_note" : ""});
    const [canEdit, setCanEdit] = React.useState(false);

    const [listSfaSpproval, setListSfaSpproval] = React.useState([]);

    const [modalReject, setModalReject] = React.useState(false);
    const [roleRejection, setRoleRejection] = React.useState(null);

    const toggleReject = (approval_role) => {
        if(approval_role){
            setRoleRejection(approval_role)
        }else{
            setRoleRejection(null)
        }
        setModalReject(!modalReject)
    };

    const handleInputApprovalDataChange = (e) => {
        const dataFormInput = {...dataApproval}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataApproval(dataFormInput)
    }

    const handleInputChange = (e) => {
        const dataFormInput = {...dataForm}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataForm(dataFormInput)
    }

    const approveCrForm = (approval_role, approval_email, approval_name, status) => {
        if(modalReject === true){
            toggleReject()
        }
        let data_approval = {...dataApproval}
        data_approval["role"] = approval_role;
        data_approval["email"] = approval_email;
        data_approval["name"] = approval_name;
        data_approval["status"] = status;
        if(status === "Approved"){
            data_approval["status_note"] = "Approved";
        }
        // console.log("data_approval", data_approval)
        props.actionApproval(data_approval)
    }

    let approvalParallelManagement = []
    if(data && data.sfa_approvals){
        approvalParallelManagement = data.sfa_approvals.filter(item => item.role !== "Change Manager")
    }

    let approvedParallelManagement = []
    if(data && data.sfa_approvals){
        approvedParallelManagement = approvalParallelManagement.filter(item => item.status !== "Approved")
    }

    let approvedChangeManager = {}
    if(data && data.sfa_approvals){
        approvedChangeManager = data.sfa_approvals.find(item => item.role === "Change Manager")
    }
    if(!approvedChangeManager){
        approvedChangeManager = {}
    }

    return (
        <React.Fragment>
            <Row>
                <Col xl={12}>
                    <Card>
                        <CardBody>
                            <GeneralDataTab data={data}  onEdit={false}/>
                        </CardBody>
                    </Card>
                </Col>
            </Row>

            <Row>
                <Col xl={12}>
                    <Card>
                        <CardBody>
                            <div className="text-center">
                                {data && data.change_category !== "Normal Minor" ? (
                                <React.Fragment>
                                    <Row >
                                        {approvalParallelManagement.map((sfaa =>
                                            <React.Fragment>
                                            <Col md={approvalParallelManagement.length > 3 ? 4 : 12  / approvalParallelManagement.length} 
                                                style={
                                                (approvalParallelManagement.length <= 2) ? 
                                                    {paddingLeft : '100px', paddingRight : '100px'}
                                                    : {paddingLeft : '30px', paddingRight : '30px'}
                                                }
                                            >
                                                <Row style={{minHeight: "50px", maxWidth: "310px", marginLeft: "auto", marginRight: "auto"}}>
                                                    <Col>
                                                        <h5>{sfaa.role}</h5>
                                                    </Col>
                                                </Row>
                                                {sfaa.status !== null && (
                                                <React.Fragment>
                                                <Row style={{maxWidth: "310px", marginLeft: "auto", marginRight: "auto"}}>
                                                    <Col xs={12}>
                                                        <span>{datetimeformatncrm(sfaa.updatedAt)}</span>
                                                    </Col>
                                                </Row>
                                                </React.Fragment>
                                                )}
                                                <Row style={{maxWidth: "310px", marginLeft: "auto", marginRight: "auto"}}>
                                                    {/* <Col xs={12}> */}
                                                    {sfaa.status === null ? (
                                                        <React.Fragment>
                                                            {(USER_PROFILE && USER_PROFILE.role && (USER_PROFILE.role.find(rl => rl === sfaa.role) || USER_PROFILE.role.find(rl => rl === "Admin") || (USER_PROFILE.title.toLowerCase().includes(sfaa.role.toLowerCase())) || (USER_PROFILE.title && USER_PROFILE.title.toLowerCase()  === sfaa.role.toLowerCase()) || (USER_PROFILE.title && USER_PROFILE.title.includes("Expert") && sfaa.role === "Domain Expert"))) ? (
                                                                <React.Fragment>
                                                                <Col md={6}>
                                                                    <Button style={{width: "100%"}} onClick={(e) => approveCrForm(sfaa.role, sfaa.email, sfaa.name, "Approved")} disabled={actionLoading}>
                                                                        {actionLoading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                                        Approve
                                                                    </Button>
                                                                </Col>
                                                                <Col md={6}>
                                                                    <Button
                                                                        color='danger'
                                                                        
                                                                        style={{width: "100%"}}
                                                                        disabled={actionLoading}
                                                                        onClick={(e) => toggleReject(sfaa.role)} 
                                                                        // disabled={loadingStatus}
                                                                    >
                                                                    {loadingStatus ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                                    Reject 
                                                                    </Button>
                                                                </Col>
                                                                </React.Fragment>
                                                            ) : (
                                                                <h2>
                                                                    NOT YET
                                                                </h2>
                                                            )}
                                                        </React.Fragment>
                                                    ) : (
                                                        <React.Fragment>
                                                        <h2 style={sfaa.status === "Approved" ? {color : "#219C90"} : sfaa.status === "Rejected" ? {color : "#D83F31"} : {}}>
                                                            {sfaa.status}
                                                        </h2>
                                                        </React.Fragment>
                                                    )}
                                                    {/* </Col> */}
                                                </Row>
                                                <React.Fragment>
                                                <Row style={sfaa.status === null ? {marginTop : '10px', maxWidth: "310px", marginLeft: "auto", marginRight: "auto"} : {maxWidth: "310px", marginLeft: "auto", marginRight: "auto"}}>
                                                    <Col xs={12}>
                                                        <span style={{fontSize : '15px'}}>{sfaa.email}</span>
                                                    </Col>
                                                </Row>
                                                </React.Fragment>
                                            </Col>
                                            </React.Fragment>
                                        ))}
                                        {data && data.sfa_approvals && approvedParallelManagement.length === 0 && (
                                            <React.Fragment>
                                                {data.current_status === "Scheduled for Approval" ? (
                                                    <Col xl={12}>
                                                        <Row>
                                                            <Col xs={12} >
                                                                <h4>Change Manager</h4>
                                                            </Col>
                                                        </Row>
                                                        <Row >
                                                            <Col xs={12}>
                                                                {(USER_PROFILE && (USER_PROFILE.title && USER_PROFILE.title.includes("Manager") && USER_PROFILE.title.includes("Change and Release")) || (USER_PROFILE.role && (USER_PROFILE.role.find(rl => rl === "Admin") || USER_PROFILE.role.find(rl => rl === "Approval NCRM")))) && (
                                                                    <React.Fragment>
                                                                        <Button 
                                                                            onClick={(e) => approveCrForm("Change Manager", null, null, "Approved")}
                                                                            disabled={actionLoading}
                                                                        >
                                                                        {actionLoading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                                        Approve
                                                                        </Button>
                                                                        <Button
                                                                            color='danger'
                                                                            style={{marginLeft : '100px'}}
                                                                            disabled={actionLoading}
                                                                            onClick={(e) => toggleReject("Change Manager")} 
                                                                            // disabled={loadingStatus}
                                                                        >
                                                                        {actionLoading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                                        Reject 
                                                                        </Button>
                                                                    </React.Fragment>
                                                                ) }
                                                                
                                                            </Col>
                                                        </Row>
                                                    
                                                    </Col>
                                                ) : (
                                                    <Col xl={12}>
                                                        {approvedChangeManager.status && (
                                                            <React.Fragment>
                                                                <Row>
                                                                    <Col xs={12} >
                                                                        <h4>Change Manager</h4>
                                                                    </Col>
                                                                </Row>
                                                                <Row>
                                                                    <Col xs={12}>
                                                                        <span>{datetimeformatncrm(approvedChangeManager.updatedAt)}</span>
                                                                    </Col>
                                                                </Row>
                                                                <Row>
                                                                    <Col xs={12}>
                                                                        <h2 style={approvedChangeManager.status === "Approved" ? {color : "#219C90"} : approvedChangeManager.status === "Rejected" ? {color : "#D83F31"} : {}}>
                                                                            {approvedChangeManager.status}
                                                                        </h2>
                                                                    </Col>
                                                                </Row>
                                                                <Row>
                                                                    <Col xs={12}>
                                                                        <span style={{fontSize : '15px'}}>{approvedChangeManager.email}</span>
                                                                    </Col>
                                                                </Row>
                                                            </React.Fragment>
                                                        ) }
                                                        
                                                        
                                                    
                                                    </Col>
                                                )}
                                                
                                            </React.Fragment>
                                        ) }
                                    </Row>
                                </React.Fragment>
                                ):(
                                    <React.Fragment>
                                        {data.current_status === "Scheduled for Approval" && (
                                            <Row>
                                                <Col xl={12}>
                                                    <Row>
                                                        <Col xs={12} >
                                                            <h4>Change Manager</h4>
                                                        </Col>
                                                    </Row>
                                                    {(USER_PROFILE && (USER_PROFILE.title && USER_PROFILE.title.includes("Manager") && USER_PROFILE.title.includes("Change and Release")) || (USER_PROFILE.role && (USER_PROFILE.role.find(rl => rl === "Admin") || USER_PROFILE.role.find(rl => rl === "Approval NCRM")))) ? (
                                                    <Row >
                                                        <Col xs={12}>
                                                        
                                                            <React.Fragment>
                                                                <Button 
                                                                    onClick={(e) => approveCrForm("Change Manager", null, null, "Approved")}
                                                                    disabled={actionLoading}
                                                                >
                                                                {actionLoading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                                Approve
                                                                </Button>
                                                                <Button
                                                                    color='danger'
                                                                    style={{marginLeft : '100px'}}
                                                                    disabled={actionLoading}
                                                                    onClick={(e) => toggleReject("Change Manager")}
                                                                    // disabled={loadingStatus}
                                                                >
                                                                {actionLoading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                                Reject 
                                                                </Button>
                                                            </React.Fragment>
                                                        </Col>
                                                    </Row>
                                                    ) : (
                                                        <React.Fragment>
                                                            <Row>
                                                                {data.current_status === "Scheduled" ? (
                                                                    <Col xs={12}>
                                                                        <h2 style={{color : "#219C90"}}>
                                                                            Approved
                                                                        </h2>
                                                                    </Col>
                                                                ) : data.current_status === "Scheduled for Approval" ? (
                                                                    <Col xs={12}>
                                                                        <h2>
                                                                            Not Yet
                                                                        </h2>
                                                                    </Col>
                                                                ) : (
                                                                    <Col xs={12}>
                                                                        <h2 style={{color : "#D83F31"}}>
                                                                            Rejected
                                                                        </h2>
                                                                    </Col>
                                                                )}
                                                                
                                                            </Row>
                                                        </React.Fragment>
                                                    )}
                                                </Col>
                                            </Row>
                                        )}
                                        
                                    </React.Fragment>
                                )}
                            </div>
                    </CardBody>
                    </Card>
                </Col>
            </Row>

            <Modal isOpen={modalReject} toggle={toggleReject} style={{marginTop : '200px'}}>
                <ModalHeader toggle={toggleReject}>Reject {data.change_id}</ModalHeader>
                <ModalBody>
                <Row>
                    <Col md={12}>
                        <div className="mb-3">
                            <Label htmlFor="reject_remark" className="form-label">Reject Remarks</Label>
                            <Input type="textarea" className="form-control" value={dataApproval.status_note} name="status_note" onChange={(e) => handleInputApprovalDataChange(e)} id="status_note" />
                        </div>
                    </Col>
                </Row>
                </ModalBody>
                <ModalFooter>
                <Button color="danger" onClick={() => approveCrForm(roleRejection, null, null, "Rejected")} >
                    Reject
                </Button>{' '}
                <Button color="secondary" onClick={toggleReject}>
                    Cancel
                </Button>
                </ModalFooter>
            </Modal>
        </React.Fragment>
    );
};

export default CRApprovalManagement;