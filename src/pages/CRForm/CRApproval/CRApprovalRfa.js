import React from 'react';

import Select from 'react-select';

import { Card, CardHeader, CardBody, Col, Container, Form, Input, Label, Row, Button, Spinner, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { dateformatncrm, datetimeformatncrm } from '../../../helpers/FunctionalHelper/date-helper';
import GeneralDataTab from '../CRDetail/GeneralDataTab';

const CRApprovalRfa = (props) => {

    const {data, USER_PROFILE, actionLoading} = props;

    const [loadingStatus, setLoadingStatus] = React.useState(false)

    const [dataForm, setDataForm] = React.useState({});
    const [dataApproval, setDataApproval] = React.useState({"status_note" : ""});
    const [canEdit, setCanEdit] = React.useState(false);

    const [modalReject, setModalReject] = React.useState(false);
    const [roleRejection, setRoleRejection] = React.useState(null);

    const toggleReject = (approval_role) => {
        if(approval_role){
            setRoleRejection(approval_role)
        }else{
            setRoleRejection(null)
        }
        setModalReject(!modalReject)
    };

    const handleInputApprovalDataChange = (e) => {
        const dataFormInput = {...dataApproval}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataApproval(dataFormInput)
    }

    const handleInputChange = (e) => {
        const dataFormInput = {...dataForm}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataForm(dataFormInput)
    }

    const approveCrForm = (approval_role, status) => {
        if(modalReject === true){
            toggleReject()
        }
        let data_approval = {...dataApproval}
        data_approval["role"] = approval_role;
        data_approval["status"] = status;
        if(status === "Approved"){
            data_approval["status_note"] = "Approved";
        }
        // console.log("data_approval", data_approval)
        props.actionApproval(data_approval)
    }

    return (
        <React.Fragment>
            <Row>
                <Col xl={12}>
                    <Card>
                        <CardBody>
                            <GeneralDataTab data={data}  onEdit={false}/>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
            
            <Row>
                <Col xl={12}>
                    <Card>
                        <CardBody>
                            <div className="text-center">
                                {data && data.change_category !== "Standard" ? (
                                <React.Fragment>
                                    <Row >
                                        {data && data.rfa_approvals && data.rfa_approvals.map((rfaa =>
                                            <React.Fragment>
                                            <Col xl={6}>
                                                <Row>
                                                    <Col xs={12} >
                                                        <h4>{rfaa.role === "QC" ? "Quality Control" : rfaa.role }</h4>
                                                    </Col>
                                                </Row>
                                                {rfaa.status !== null && (
                                                <React.Fragment>
                                                <Row>
                                                    <Col xs={12}>
                                                        <span>{datetimeformatncrm(rfaa.updatedAt)}</span>
                                                    </Col>
                                                </Row>
                                                </React.Fragment>
                                                )}
                                                <Row>
                                                    <Col xs={12}>
                                                    {rfaa.status === null ? (
                                                        <React.Fragment>
                                                            {(USER_PROFILE && USER_PROFILE.role && (USER_PROFILE.role.find(rl => rl === rfaa.role) || USER_PROFILE.role.find(rl => rl === "Admin"))) ? (
                                                                <React.Fragment>
                                                                    {data.current_status === "Request for Authorization" && (
                                                                        <React.Fragment>
                                                                        <Button onClick={(e) => approveCrForm(rfaa.role, "Approved")} disabled={actionLoading}>
                                                                            {actionLoading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                                            Approve
                                                                        </Button>
                                                                        <Button
                                                                            onClick={(e) => toggleReject(rfaa.role)} 
                                                                            color='danger'
                                                                            style={{marginLeft : '100px'}}
                                                                            disabled={actionLoading}
                                                                            // disabled={loadingStatus}
                                                                        >
                                                                        {actionLoading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                                        Reject 
                                                                        </Button>
                                                                        </React.Fragment>
                                                                    )}
                                                                </React.Fragment>
                                                            ) : (
                                                                <h2>
                                                                    NOT YET
                                                                </h2>
                                                            )}
                                                        </React.Fragment>
                                                    ) : (
                                                        <React.Fragment>
                                                        <h2 style={rfaa.status === "Approved" ? {color : "#219C90"} : rfaa.status === "Rejected" ? {color : "#D83F31"} : {}}>
                                                            {rfaa.status}
                                                        </h2>
                                                        </React.Fragment>
                                                    )}
                                                    </Col>
                                                </Row>
                                                {rfaa.status !== null && (
                                                <React.Fragment>
                                                <Row>
                                                    <Col xs={12}>
                                                        <span>{rfaa.email}</span>
                                                    </Col>
                                                </Row>
                                                </React.Fragment>
                                                )}
                                            </Col>
                                            </React.Fragment>
                                        ))}
                                    </Row>
                                </React.Fragment>
                                ):(
                                <React.Fragment>
                                    <Row>
                                        <Col xl={12}>
                                            <Row>
                                                <Col xs={12} >
                                                    <h4>Change Coordinator</h4>
                                                </Col>
                                            </Row>
                                            <Row >
                                                <Col xs={12}>
                                                    <Button 
                                                        onClick={(e) => approveCrForm("Change Coordinator", "Approved")} 
                                                        disabled={actionLoading}
                                                    >
                                                    {actionLoading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                    Approve
                                                    </Button>

                                                    {/* <Button
                                                        color='danger'
                                                        style={{marginLeft : '100px'}}
                                                        onClick={(e) => toggleReject(rfaa.role)} 
                                                        disabled={actionLoading}
                                                    >
                                                    {loadingStatus ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null }
                                                    Reject 
                                                    </Button> */}
                                                </Col>
                                            </Row>
                                        
                                        </Col>
                                    </Row>
                                </React.Fragment>
                                )}
                            </div>
                        </CardBody>
                    </Card>
                </Col>
            </Row>

            <Modal isOpen={modalReject} toggle={toggleReject} style={{marginTop : '200px'}}>
                <ModalHeader toggle={toggleReject}>Reject {data.change_id}</ModalHeader>
                <ModalBody>
                <Row>
                    <Col md={12}>
                        <div className="mb-3">
                            <Label htmlFor="reject_remark" className="form-label">Reject Remarks</Label>
                            <Input type="textarea" className="form-control" value={dataApproval.status_note} name="status_note" onChange={(e) => handleInputApprovalDataChange(e)} id="status_note" />
                        </div>
                    </Col>
                </Row>
                </ModalBody>
                <ModalFooter>
                <Button color="danger" onClick={() => approveCrForm(roleRejection, "Rejected")} >
                    Reject
                </Button>{' '}
                <Button color="secondary" onClick={toggleReject}>
                    Cancel
                </Button>
                </ModalFooter>
            </Modal>
        </React.Fragment>
    );
};

export default CRApprovalRfa;