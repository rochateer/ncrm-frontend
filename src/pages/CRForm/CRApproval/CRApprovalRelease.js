import React from 'react';

import Select from 'react-select';

import { Card, CardHeader, CardBody, Col, Container, Form, Input, Label, Row, Button, Spinner, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';

import { dateformatncrm } from '../../../helpers/FunctionalHelper/date-helper';

import { API_CLIENT_AUTH_BEARER } from "../../../helpers/api-helper";
import PaginationTable from '../../../Components/TableComponent/PaginationTable';

const apiAuth = new API_CLIENT_AUTH_BEARER();

const CRApprovalRelease = (props) => {

    const {data, USER_PROFILE} = props;

    const [loadingStatus, setLoadingStatus] = React.useState(false);

    const [dataApproval, setDataApproval] = React.useState({"status_note" : "Approved"});

    const [listUserApproval, setListUserApproval] = React.useState([]);

    const [cabMom, setCabMom] = React.useState('');

    const [showAddApproval, setShowAddApproval] = React.useState(false);

    const [dataUser, setDataUser] = React.useState([]);
    const [pageUser, setPageUser] = React.useState(1);
    const [totalDataUser, setTotalDataUser] = React.useState(0);
    const [perPageUser, setPerPageUser] = React.useState(10);

    const [dataForm, setDataForm] = React.useState({});
    const [canEdit, setCanEdit] = React.useState(false);

    const onChangePageUser = (e) => {
        setPageUser(e);
      }

    React.useEffect(() => {
        getDataUser()
    }, []);

    const getDataUser = async() => {
        const res = await apiAuth.get('/getUser?lmt=10&pg='+pageUser);
        if(res && res.data){
            setDataUser(res.data.data)
            setTotalDataUser(res.data.totalResults)
        }
    }

    const handleInputChange = (e) => {
        const dataFormInput = {...dataForm}
        const {name, value} = e.target
        dataFormInput[name] = value;
        setDataForm(dataFormInput)
    }

    const addApproval = () => {
        let list_approval = [...listUserApproval];
        list_approval.push({})
        setListUserApproval(list_approval)
    }

    const approveCrForm = (approval_role) => {
        let data_approval = {...dataApproval}
        data_approval["role"] = approval_role;
        data_approval["approval_sfa"] = listUserApproval.map(la => ({"role" : la.title, "email" : la.email, "name" : [la.first_name, la.last_name].join(" ")}));
        // console.log("data_approval", data_approval)
        data_approval["status"] = "Approved";
        data_approval["cab_mom"] = cabMom;
        props.actionApproval(data_approval);
    }

    const handleInputMoMChange = (e) => {
        let cab_mom = cabMom
        const {name, value} = e.target
        cab_mom = value
        setCabMom(cab_mom)
    } 

    const handleCheckboxUserChange = (e, id, value) => {

        let list_approval = [...listUserApproval]

        const {checked} = e.target;

        if(checked){
            const data_checked = dataUser.find(du => du.email === id);
            list_approval.push(data_checked)
        }else{
            list_approval = list_approval.filter(du => du.email !== id )
        }

        setListUserApproval(list_approval);
    }

    const handleCloseCrApproval = () => setShowAddApproval(false);
    const onClickAddCrApproval = () => setShowAddApproval(true);

    return (
        <React.Fragment>
            <Row>
                <Col md={6}>
                    <Row>
                        <Col md={8}>
                            <div className="mb-3">
                                <Label htmlFor="oc_domain_category" className="form-label">Domain Category</Label>
                                <Input type="text" className="form-control" value={data.oc_domain_category} name="oc_domain_category" disabled onChange={(e) => handleInputChange(e)} id="oc_domain_category" />
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <div className="mb-3">
                                <Label htmlFor="reason_for_change" className="form-label">Reason For Change</Label>
                                <Input type="text" className="form-control" value={data.reason_for_change} name="reason_for_change" disabled onChange={(e) => handleInputChange(e)} id="reason_for_change" />
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <div className="mb-3">
                                <Label htmlFor="description_of_change" className="form-label">Description Of Change</Label>
                                <Input type="textarea" className="form-control" value={data.description_of_change} name="description_of_change" disabled onChange={(e) => handleInputChange(e)} id="description_of_change" />
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <div className="mb-3">
                                <Label htmlFor="mom_cab" className="form-label">MOM CAB</Label>
                                <Input type="textarea" className="form-control" value={cabMom} name="mom_cab" disabled onChange={(e) => handleInputMoMChange(e)} id="mom_cab" />
                            </div>
                        </Col>
                    </Row>
                </Col>
                <Col md={6}>
                    <Row>
                        <Col md={12}>
                            <div className="mb-3">
                                <Label htmlFor="changeType" className="form-label">Region</Label>
                                <Select
                                    isDisabled={true}
                                    isClearable={true}
                                    value={data.region && data.region.map(item => ({value : item, label : item}) )}
                                    isMulti={true}
                                    options={data.region && data.region.map(item => ({value : item, label : item}) )}
                                />
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <div className="mb-3">
                                <Label htmlFor="changeType" className="form-label">Responsible Approval</Label>
                                <Select
                                    isDisabled={true}
                                    isClearable={true}
                                    value={data.responsible_approval && data.responsible_approval.map(item => ({value : item, label : item}) )}
                                    isMulti={true}
                                    options={data.responsible_approval && data.responsible_approval.map(item => ({value : item, label : item}) )}
                                />
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Row>

        </React.Fragment>
    );
};

export default CRApprovalRelease;