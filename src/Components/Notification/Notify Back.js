import React from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { useSelector, useDispatch } from "react-redux";
import { Col, Row, Pagination, PaginationItem, PaginationLink} from 'reactstrap';
import { notificationFlagDelete } from '../../redux/reducers/notificationFlagReducer';
import { createSelector } from 'reselect';

const toastifyNotificationConfig = {
    position: "top-right", 
    hideProgressBar: true, 
    closeOnClick: true, 
    autoClose: false
};

const ShowNotificationFlags = (props) => {

    console.log("Show", props)

    const {alert_message, alert_status, idx, onFlagClose} = props

    const defaultNotify = (alert_message) => {
        toast(alert_message, { onClose: () => onFlagClose(), ...toastifyNotificationConfig, className: 'bg-primary text-white' })
    };
    const successNotify = (alert_message, idx) => {
        toast(alert_message, { onClose: () => onFlagClose(), ...toastifyNotificationConfig, className: 'bg-success text-white' })
    };
    const warningNotify = (alert_message, idx) => {
        toast(alert_message, { onClose: () => onFlagClose(), ...toastifyNotificationConfig, className: 'bg-warning text-white' })
    };
    const errorNotify = (alert_message, idx) => {
        toast(alert_message, { onClose: () => onFlagClose(), ...toastifyNotificationConfig, className: 'bg-danger text-white' })
    };

    if(alert_status === "default"){
        const message_flag = alert_message ? alert_message : "Welcome Back! This is a Toast Notification";
        defaultNotify(message_flag, idx);
        return <ToastContainer />
    }
    if(alert_status === "success"){
        const message_flag = alert_message ? alert_message : "Your application was successfully sent";
        successNotify(message_flag, idx)
        return <ToastContainer />
    }
    if(alert_status === "warning"){
        const message_flag = alert_message ? alert_message : "Warning ! Something went wrong try again";
        warningNotify(message_flag, idx);
        return <ToastContainer />
    }
    if(alert_status === "error"){
        const message_flag = alert_message ? alert_message : "Error ! An error occurred.";
        errorNotify(message_flag, idx);
        return <ToastContainer />
    }else{
        return <ToastContainer />
    }
}

const NotificationFlagComponent = () => {

    const [notifyAlert, setNotifyAlert] = React.useState([]);

    const dispatch = useDispatch()
    const reducerNoficationFlag = useSelector(state => state.NotificationFlag)

    // const selectReducerNotificationFlag = (state) => state.NotificationFlag;
    // const reducerNotificationFlag = createSelector(
    //     selectReducerNotificationFlag,
    //     (notif) => ({
    //         notification_alert: notif.notification_alert
    //     })
    // );

    // const {
    //     notification_alert
    // } = useSelector(reducerNotificationFlag);

    console.log("reducerNoficationFlag", reducerNoficationFlag)

    const removeNotificationFlag = (idx) => {
        console.log("test notify del")
        dispatch(notificationFlagDelete(idx))
    }

    React.useEffect(() => {
        setNotifyAlert(reducerNoficationFlag.notification_alert)
    }, []);

    console.log("notifyAlert", notifyAlert)

    return(
        <React.Fragment>
            {notifyAlert.map((notif, idx) => {
                <React.Fragment>
                    {console.log("test idx", idx)}
                <ShowNotificationFlags 
                    alert_status = {notif.alert_status}
                    alert_message = {notif.alert_message}
                    onFlagClose = {() => removeNotificationFlag(idx)}
                />
                </React.Fragment>
            })}
            <ToastContainer />
        </React.Fragment>
    )
}

export default NotificationFlagComponent;