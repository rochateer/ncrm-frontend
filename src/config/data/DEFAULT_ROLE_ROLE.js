export const DEFAULT_ROLE_ROLE = [
    { value: 'Admin', label: 'Admin' },
    { value: 'Change Coordinator', label: 'Change Coordinator' },
    { value: 'Requestor', label: 'Requestor' },
    { value: 'Line Manager', label: 'Line Manager' },
    { value: 'Change Manager', label: 'Change Manager' },
    { value: 'Approval NCRM', label: 'Approval NCRM' },
    { value: 'Approval Regional', label: 'Approval Regional' },
    { value: 'Approval Expert Domain', label: 'Approval Expert Domain' },
];