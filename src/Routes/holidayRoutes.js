import Holiday from "../pages/HolidayDateManagement/index";

const holidayRoutes = [
    { path: "/holiday-date-management", component: <Holiday /> },
];

export { holidayRoutes };