import Maintenance from "../pages/MaintenancePages/indexMaintenance";

const maintenanceRoutes = [
    { path: "/under-maintenance", component: <Maintenance /> },
];

export { maintenanceRoutes };