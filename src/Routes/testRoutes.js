import TestPageList from '../pages/TestPage/TestPageList';

const testRoutes = [
    { path: "/test-page", component: <TestPageList /> },
];

export { testRoutes };